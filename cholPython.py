
from os.path import dirname, join as pjoin
import scipy.io as sio
import scipy
from scipy import sparse 
from scipy.sparse import linalg 
import os
import psutil
import numpy as np
import time


def printTime(time):
    if time < 60:
        return str('%.2f'%(time))+" seconds"
    else:
        min = time/60
        if min < 60:
            return str('%.2f'%(min))+" minutes"
        else:
              return str('%.2f'%(min/60))+" hours"    



location = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__)))
matrixName= 'cfd2.mat'
FILEPATH = os.path.join(location, 'Matrici/'+matrixName)
matrix_data = sio.loadmat(FILEPATH)
matrix = matrix_data['Problem']['A'][0][0]
dimension = matrix.shape[0]
file = open("Results_Python/" + matrixName,'w') 
file.write("matrix: "+ str(matrixName) + "\n")
file.write("dimension of matrix: " + str(dimension)+ "\n\n")
errors = []
times = []
memorys = []


for i in range (1,2):
    start = time.time()
    cholFactor = scipy.sparse.linalg.factorized(matrix)
    ones = np.ones((dimension,1))
    b = matrix*ones
    x = cholFactor(b)
    error = np.linalg.norm(ones - x)/np.linalg.norm(ones)
    errors.append(error)
    end = time.time()
    iterationTime = end - start
    times.append(iterationTime)
    print(iterationTime)
    """
    process = psutil.Process(os.getpid())
    memoryUsage = process.memory_info().rss/1000000
    memorys.append(memoryUsage)

    file.write("iteration: " + str(i) + "\n")
    file.write("error: " + str(error)+ "\n")
    file.write("time elapsed: " + str(iterationTime)+ "\n")
    file.write("time human: " + str(printTime(iterationTime))+ "\n")
    file.write("memory used: " + str(memoryUsage) + " MB"+ "\n \n" )
    """
meanError = np.mean(errors)
varianceError = np.std(errors)
meanTime = np.mean(times)
varianceTime = np.std(times)
#meanMemory = np.mean(memorys)
#varianceMemory = np.std(memorys)
file.write("\n\nMean error: " + str(meanError) + "\n Error variance " + str(varianceError))
file.write("\n\nMean time: " + str(meanTime) + " s\n Time variance " + str(varianceTime))
#file.write("\n\nMean memory: " + str(meanMemory) + " MB\n Memory variance " + str(varianceMemory) + " MB")        
