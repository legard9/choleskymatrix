#!/usr/bin/python2.7

from os.path import dirname, join as pjoin
import scipy.io as sio
import scipy
from scipy import sparse 
from scipy.sparse import linalg 
import os
import psutil
import numpy as np
import time
import subprocess
from sksparse.cholmod import cholesky

def printTime(time):
    if time < 60:
        return str('%.2f'%(time))+" seconds"
    else:
        min = time/60
        if min < 60:
            return str('%.2f'%(min))+" minutes"
        else:
              return str('%.2f'%(min/60))+" hours"    



location = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__)))
matrixName= 'cfd1.mat'
FILEPATH = os.path.join(location, 'Matrici/'+matrixName)
matrix_data = sio.loadmat(FILEPATH)
matrix = matrix_data['Problem']['A'][0][0]
dimension = matrix.shape[0]
file = open("Results_Python/" + matrixName,'w') 
file.write("matrix: "+ str(matrixName) + "\n")
file.write("dimension of matrix: " + str(dimension)+ "\n\n")

start = time.time()
cholFactor = cholesky(matrix)
ones = np.ones((dimension,1))
b = matrix*ones
x = cholFactor(b)
error = np.linalg.norm(ones - x)/np.linalg.norm(ones)

iterationTime = time.time() - start
process = os.getpid()
result = subprocess.run(['grep', 'VmHWM','/proc/'+str(process)+'/status' ], stdout=subprocess.PIPE)
memory = result.stdout


file.write("\n\nError: " + str(error))
file.write("\n\nTime: " + str(iterationTime))
file.write("\n\nMemory: " + str(memory))        

file.close()