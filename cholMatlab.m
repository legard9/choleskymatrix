clc; clear; close all;
matrixData = importdata('Matrici/Flan_1565.mat');

matrix = matrixData.A;

dim = size(matrix, 1);
ex = ones(dim, 1);
b = matrix*ex;

tic

x=matrix\b;
elapsed_time = toc
error = norm(ex-x)/norm(ex)
    
pid= feature("getpid")

res = system(strcat("grep VmHWM /proc/",num2str(pid),"/status"));
